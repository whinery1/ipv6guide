# ipv6guide

As there are no current books on IPv6 this project aims to create a guide to implementing IPv6. Lets start with the basics and see where we are going.

## The goal

Write a book style document about IPv6 in a collaborative fashion which can be kept up to
date.


## How to contribute?

You can contribute in several ways. The most important is probably to add content to
this document. Then content needs to be reviewed and edited. Also some nice graphics an
illustrations would be great.

You can to this either by creating merge / pull request, open tickets when you find
mistakes or want something to be added.

If you plan on making larger or more contributions please request to join the project
directly.

## Why gitlab and not github?

At the time we started this github.com had no AAAA record. Want to host an IPv6 project on an IPv4 only site?

## Content

- Intro
    - Why?
    - History
    - Addresses
- Planing
    - Address Planning
    - Dual-Stack vs Single Stack
- Operating Systems
    - Linux
        - Debian
        - Ubuntu
        - RH
    - *BSD
    - Mac
    - Windows
- Networking
    - DNS
    - DHCPv6
    - OSPFv3
    - BGP
    - EIGRP?
    - RIPnG?
    - ISIS
- Security
    - Layer2 Security
    - Packet filtering
    - Firewalls
- Cloud
- Transition Mechanisms
- Software Development
